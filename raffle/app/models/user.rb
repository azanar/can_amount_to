class User < ActiveRecord::Base
  has_many :tickets, :dependent => :destroy
  has_many :raffles, :dependent => :destroy
end
