class Ticket < ActiveRecord::Base
  belongs_to :raffle
  belongs_to :user

  validates :raffle, :user, :value, :presence => true

  validate :raffle_can_not_be_users_own
  validate :value_must_be_positive
  validate :tickets_must_be_available

  attr_accessible :value, :user

  def raffle_can_not_be_users_own
    if !raffle.nil? and !user.nil? and raffle.user == user
      errors.add(:user, "can not buy a ticket to his own raffle")
    end
  end

  def tickets_must_be_available
    if !raffle.nil? and raffle.tickets_remaining < value
      errors.add(:raffle, "does not have enough tickets remaining")
    end
  end

  def value_must_be_positive
    if !value.nil? and value <= 0
      errors.add(:value, "of ticket can not be negative")
    end
  end
  
end

