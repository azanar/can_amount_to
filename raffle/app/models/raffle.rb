class Raffle < ActiveRecord::Base
  has_many :tickets
  belongs_to :user

  attr_accessible :limit, :user

  validates :limit, :user, :presence => true

  def tickets_remaining
    limit - tickets_issued
  end

  def tickets_issued
    tickets.sum("value")
  end
  
  def choose_winner
    if tickets_issued == 0
      return nil # you can't win if you don't play
    end

    tickets_left = rand(tickets_issued)
    tickets.order("id").all.find do |t|
      if tickets_left - t.value < 0
        t
      else
        tickets_left -= t.value
        nil
      end
    end
  end
end

