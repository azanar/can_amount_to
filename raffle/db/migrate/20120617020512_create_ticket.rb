class CreateTicket < ActiveRecord::Migration
  def up
    create_table :tickets do |t|
      t.integer :value
      t.integer :raffle_id
      t.integer :user_id
    end
  end

  def down
    drop_table :tickets
  end
end
