class CreateRaffle < ActiveRecord::Migration
  def up
    create_table :raffles do |t|
      t.integer :limit
      t.integer :user_id
    end
  end

  def down
    drop_table :raffle
  end
end
