DENOMINATION_ORDER = [:quarters, :dimes, :nickels, :pennies]
COIN_VALUE = {:quarters => 0.25,
              :dimes => 0.10,
              :nickels => 0.05,
              :pennies => 0.01}

DELTA = 0.001

# this function takes a hashtable representing coin amounts, and
# an amount we are aiming to get to. It will then recursively prune
# the greatest coin denomination from the list, and pass that along
# with each feasible quantity of that coin denomination in turn to
# another call of can_amount_to? until either success is returned,
# or the quantity is exhausted.
def can_amount_to?(coins, amount)

  # if we have no more coins to distribute, so it's time to make a determination
  if (coins.empty?)
    return amount.abs < DELTA
  end

  # find the greatest denomination we still have to work with
  denom_of_interest = DENOMINATION_ORDER.find { |denom| coins.has_key?(denom) }

  my_value = COIN_VALUE[denom_of_interest]

  # start from however many coins don't put us over our needed amount.  
  # XXX: the round call in this line is kind of lame, but missing it can cause us to pick one fewer coins than we should.
  start = [coins[denom_of_interest], (amount / my_value).round(2).floor].min

  coins_without_self = coins.reject {|k,_| k == denom_of_interest}

  # see if any amount of myself can lead the rest of the change to balancing out.
  start.downto(0).any? do |amt|
    deducted_amount = amount - my_value * amt

    can_amount_to?(coins_without_self , deducted_amount)
  end
end

# test harness. Feel free to adjust the knobs here. :-)
1.upto(200) do |amt_in_cents|
  puts "---\n"
  puts "#{(amt_in_cents/100.0).round(2)}"
  puts can_amount_to?({:quarters => 5, :dimes => 1, :nickels => 3, :pennies => 2}, (amt_in_cents/100.0).round(2)) 
end
